<?php
class Npcdb2SpController extends AppController {

	var $name = 'Npcdb2Sp';
	#var $helpers = array('Html', 'Form');
	var $components = array( 'RequestHandler', 'Security' );
	var $uses = '';
	
	# WSDL Specific Options
	#var $wsdl = 'https://m2mnpbh.test.systor.st/services/NpcdbService?wsdl';
	var $wsdl = 'http://apps.2connectbahrain.com/cake139/np/wsdl/SpService/npcdb2sp.wsdl';
	var $wsdl_options=array(
		'trace'=>1,
		'cache_wsdl'=>'WSDL_CACHE_NONE',
		#'exceptions'=>0
	);
	
	
	function beforeFilter() {
		$this->Security->loginOptions = array(
			'type'=>'basic',
			#'realm'=>'realm'
		);
		$this->Security->loginUsers = array(
			'soap_conb'=>'ydSp7248',
			'soap_csys'=>'sR9rMpy5'
		);
		$this->Security->validatePost = false;	# Required for non-model field submissions
		$this->Security->requireLogin();
	}

	
	function service() {
		#$this->log( print_r($this->data,true), 'debug' );
		$this->layout = false;
		$this->autoRender = false;
		$this->RequestHandler->respondAs('xml');
		$server = new SoapServer($this->wsdl, $this->wsdl_options);
		$server->SetClass("Npcdb2SpController");
		#$this->log( print_r($server->getFunctions(),true), 'debug' );
		$server->handle();
	}
	
	
	function HandleNpRequest( $data ) {
		#$this->log( print_r($data,true), 'debug' );
		#return $data;
		return $this->SendNpRequestAck($data);
	}
	
	
	function SendNpRequest($data) {
		return $this->SendNpRequestAck();
	}
	
	
	function SendNpRequestAck($data) {
		$obj = new stdClass();
		$obj->ServiceType = $data->ServiceType;
		$obj->MessageCode = 'NpRequestAck';
		$obj->PortID = $data->PortID;
		$obj->OriginationID = 'CONB';
		$obj->DestinationID = 'CSYS';
		return $obj;
	}
	
	
	function SendNpRequestAccept() {
		return $this->SendNpRequestAcceptAck();
	}
	
	
	function SendNpRequestAcceptAck() {
		$obj = new stdClass();
		$obj->ServiceType = 'F';
		$obj->MessageCode = 'NpRequestAcceptAck';
		$obj->Number = 17500110;
		$obj->PortID = 'CONB-BTCF-20110526-162815';
		$obj->SubmissionID = 'CONB-1234-12345678';
		$obj->DonorID = 'BTCF';
		$obj->RecipientID = 'CONB';
		$obj->OriginationID = 'CONB';
		$obj->DestinationID = 'CSYS';
		return $obj;
	}
	
	
}
?>