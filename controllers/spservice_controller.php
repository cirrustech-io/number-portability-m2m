<?php
class SpServiceController extends AppController {

	var $name = 'SpService';
	var $components = array( 'RequestHandler', 'Email', 'Security' );
	var $uses = ''; 	# No Model Required
	
	# Test filename storage
	var $filename_body = '';
	
	# WSDL Specific Options
	var $wsdl = '';
	var $wsdl_options = array();

	
	function beforeFilter() {
		/*
		 * Required for HTTP BASIC Authorization
		 */
		$this->Security->loginOptions = array(
			'type'=>'basic',
			#'realm'=>'realm'
		);
		$this->Security->loginUsers = array(
			'soap_conb'=>'ydSp7248',
			'soap_csys'=>'sR9rMpy5'
		);
		$this->Security->validatePost = false;	# Required for non-model field submissions
		$this->Security->requireLogin();
		/*
		 * End of HTTP BASIC Authorization code
		 */
		
		
		/*
		 * This beforeFilter block had to be created as Router::url would not work in
		 * the class variable. 
		 * 
		 * var $wsdl = Router::url('/',true)
		 */
		
		#$this->wsdl = Router::url('/',true) . 'wsdl/SpService/npcdb2sp.wsdl';
		$this->wsdl = 'https://np.2connectbahrain.com/wsdl/SpService/npcdb2sp.wsdl';
		#$this->log( $this->wsdl, 'debug' );
		$this->wsdl_options=array(
			'trace'=>1,
			'cache_wsdl'=>'WSDL_CACHE_NONE',
		);
	
	}

	
	function index() {
		$this->autoRender = false;
	}
	
	
	function service() {
		$this->layout = false;
		$this->autoRender = false;
		$this->RequestHandler->respondAs('xml');
		$server = new SoapServer($this->wsdl, $this->wsdl_options);
		$server->SetClass("SpServiceController");
		$server->handle();
		$this->log( "spservice-service accessed", 'debug' );
	}
	
	
	function SendDummyFault($data){
		/*
		 * Dummy function to return notification message to NPCDB
		 */
		
		$this->log( "Sending Dummy Fault for " . $data->PortID, 'debug' );
		
		# Generate message
		$fault = new stdClass();
		$fault->ErrorNotification = new stdClass();
		$fault->ErrorNotification->MessageCode = 'ErrorNotification';
		$fault->ErrorNotification->PortID = $data->PortID;
		$fault->ErrorNotification->RejectedMessageCode = $data->MessageCode;
		$fault->ErrorNotification->ErrorCode = 'ERR0099';
		$fault->ErrorNotification->Comments = 'Development Phase: The raw XML data has been captured and saved. Other functionality will be added later';
		$fault->ErrorNotification->OriginationID = 'CONB';
		$fault->ErrorNotification->DestinationID = 'CSYS';
		
		# Send message
		throw new SoapFault( 
			$fault->ErrorNotification->ErrorCode, 
			$fault->ErrorNotification->Comments, 
			null, 
			$fault 
			);
	}
	

	function getallheaders()
	{
		$headers = '';

		if (!function_exists('getallheaders'))
		{

			foreach ($_SERVER as $name => $value)
			{
				if (substr($name, 0, 5) == 'HTTP_')
				{
					$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
				}
			}
		}
		else
		{
			$headers = getallheaders();
		}

		return $headers;
	}

	
	function SaveToLocalStorage($data) {
		/*
		 * Save raw data in to text files on local storage
		 * Ali Khalil 20110601
		 */
		try {
			$storage_location = '/data/var/www/m2m-dumps/';
			#$storage_location = TMP . 'dump/';
			
			$filename_base = date('YmdHis') . '-' . $data->MessageCode . '-' . $data->RecipientID;
			$filename_headers = $filename_base . '-headers.txt';
			$filename_body = $filename_base . '-body.xml';
			
			# Generate storage paths
			$file_headers = $storage_location . 'headers/' . $filename_headers;
			$file_body = $storage_location . 'body/' . $filename_body;
			
			# Write headers to file
			$fh_headers = fopen( $file_headers, 'w' );
			fwrite( $fh_headers, print_r( $this->getallheaders(), true ) );
			fclose($fh_headers);
			
			# Write RAW XML to File
			$fh_body = fopen( $file_body, 'w' );
			fwrite( $fh_body, print_r( $GLOBALS['HTTP_RAW_POST_DATA'], true ) );
			fclose($fh_body);
			$this->log( "saveToLocation - Filename (Body): " . $filename_body, 'debug' );
			return $filename_body;
		}catch( Exception $e ) {
			return $e->getMessage();
		}
		
		
	}
	
	
	function HandleNpRequest($data){
		$this->log( 'HandleNpRequest:', 'debug' );
		$this->log( print_r( $data,true), 'debug' );
		
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
			
			# Set the Response Parameters
			$params = array(
				"ServiceType" => $data->ServiceType,
				'MessageCode' => 'NpRequestAck',
				'PortID' => $data->PortID,
				'OriginationID' => $data->DestinationID,
				'DestinationID' => $data->OriginationID,
			);

			$this->log( 'HandleNpRequest - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );

			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
		
	}
	
	
	function HandleNpRequestAccept($data){
		$this->log( 'HandleNpRequestAccept:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpRequestAcceptAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'HandleNpRequestAccept - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
		
	}
	
	
	function HandleNpRequestReject($data){
		$this->log( 'HandleNpRequestReject:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpRequestRejectAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'HandleNpRequestReject - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
		return $this->SendDummyFault($data);
	}
	
	
	function HandleNpRequestCancel($data){
		$this->log( 'HandleNpRequestCancel:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpRequestCancelAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => $data->OriginationID,
			);
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			

			$this->log( 'HandleNpRequestCancel - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			return $params;
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function HandleNpExecuteBroadcast($data){
		$this->log( 'HandleNpExecuteBroadcast:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpExecuteBroadcastAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpExecuteBroadcastAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			#$this->sendEmail( $data, $filename );
			$this->log( 'HandleNpExecuteBroadcast - Email notification intentionally skipped.', 'debug' );
			
			# Add record to MySQL database
			$this->requestAction( 'ported_numbers/insert', array('data'=>$data));
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function HandleNpExecuteComplete($data){
		$this->log( 'HandleNpExecuteComplete:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpExecuteCompleteAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpExecuteCompleteAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
		
	}
	
	
	function HandleNpDeactivateBroadcast($data){
		$this->log( 'HandleNpDeactivateBroadcast:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		#exit;
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				'ServiceType' => $data->ServiceType,
				'MessageCode' => 'NpDeactivateBroadcastAck',
				'Number' => $data->Number,
				'PortID' => $data->PortID,
				'SubscriptionNetworkID' => $data->SubscriptionNetworkID,
				'BlockID' => $data->BlockID,
				'OriginationID' => $data->DestinationID,
				'DestinationID' => 'CSYS',
			);

			$this->log( 'NpDeactivateBroadcastAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Remove record from MySQL database
			$this->requestAction( 'ported_numbers/remove', array('data'=>$data));
			
			# Send an email notification
			#$this->sendEmail( $data, $filename );
			$this->log( 'HandleNpDeactivateBroadcast - Email notification intentionally skipped.', 'debug' );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function HandleNpDeactivateComplete($data){
		$this->log( 'HandleNpDeactivateComplete:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpDeactivateCompleteAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpDeactivateCompleteAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
		
	}
	
	
	function HandleNpQueryComplete($data){
		#$this->log( "NpQueryComplete - Filename (Body): " . $this->filename_body, 'debug' );
		$this->log( 'HandleNpQueryComplete:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpQueryCompleteAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpQueryCompleteAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			#$this->log( print_r( $filename, true), 'debug' );
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function HandleNpBillingResolution($data){
		$this->log( 'HandleNpBillingResolution:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpBillingResolutionAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpBillingResolutionAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function HandleNpBillingResolutionReceived($data){
		$this->log( 'HandleNpBillingResolutionReceived:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
		
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpBillingResolutionReceivedAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpBillingResolutionReceivedAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
		
	}
	
	
	function HandleNpBillingResolutionEnd($data){
		$this->log( 'HandleNpBillingResolutionEnd:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
	
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpBillingResolutionEndAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpBillingResolutionEndAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function HandleNpBillingResolutionAlert($data){
		$this->log( 'HandleNpBillingResolutionAlert:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
	
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpBillingResolutionAlertAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpBillingResolutionAlertAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function HandleNpBillingResolutionAlertReceived($data){
		$this->log( 'HandleNpBillingResolutionAlertReceived:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
	
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "NpBillingResolutionAlertReceivedAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'NpBillingResolutionAlertReceivedAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function HandleErrorNotification($data){
		$this->log( 'HandleErrorNotification:', 'debug' );
		$this->log( print_r( $data, true), 'debug' );
	
		try{
			# Save the Headers and POST data to files on local storage
			$filename = $this->SaveToLocalStorage($data);
		
			$this->autoRender = false;
			
			$params = array(
				"ServiceType" => $data->ServiceType,
				"MessageCode" => "ErrorNotificationAck",
				"PortID" => $data->PortID,
				"OriginationID" => $data->DestinationID,
				"DestinationID" => "CSYS",
			);

			$this->log( 'ErrorNotificationAck - Parameters:', 'debug' );
			$this->log( print_r( $params, true), 'debug' );
			
			# Send an email notification
			$this->sendEmail( $data, $filename );
			
			return $params;
			
		}catch(Exception $e){
			$this->log( print_r( $e->getMessage(), true), 'debug' );
			return $this->SendDummyFault($data);
		}
	}
	
	
	function sendEmail( $data, $filename ) {
		# Send an email to distribution list
		# TODO: Generate body in this action
		$this->log( 'Filename (Body) - sendEmail: ' . $filename, 'debug' );
		$email_body = "An " . $data->MessageCode . " message has been received from NPCDB.\n\n";
		foreach( $data as $key => $value )
			$email_body .= sprintf("%-21s", $key . ":") . $value . "\n";
		$email_body .= "\n\nPlease log in to the Web Portal at https://www.npcs.bh/ and update the request if necessary.";
		$email_body .= "\n\nView raw message here: " . Router::url('/',true) . "dumps/body/" . $filename;
		#$this->log( 'Email (Body) - sendEmail: ' . $email_body, 'debug' );
		try {
			$this->Email =& new EmailComponent(null);
			$this->Email->smtpOptions = array(
				'port' => '25',
				'timeout' => '30',
				'host' => 'mail.2connectbahrain.com',
				'client' => 'np.2connectbahrain.com',
			);
			$this->Email->delivery = 'smtp';
			$this->Email->lineLength = 250;
			$this->Email->to = 'number-portability@2connectbahrain.com';
			$this->Email->bcc = 'ali.khalil@2connectbahrain.com';
			$this->Email->subject = 'NPCDB: ' . $data->MessageCode . ' - ' . $data->PortID;
			$this->Email->replyTo = 'noreply@2connectbahrain.com';
			$this->Email->from = 'number-portability@2connectbahrain.com';
			#$this->Email->template = 'simple_message';
			$this->Email->sendAs = 'text';
			$this->log( 'Sending Email...', 'debug' );
			$this->Email->send($email_body); 
			$this->log( 'Email sent successfully.', 'debug' );
		} catch ( Exception $e ) {
			return $e->getMessage();
		}
	}
}
?>
