<?php
class ServicesController extends AppController {

	var $name = 'Services';
	#var $helpers = array('Html', 'Form');
	var $components = array( 'RequestHandler', 'Security' );
	
	# WSDL Specific Options
	var $wsdl = '';
	var $wsdl_options = array();
	
	
	function beforeFilter() {
		/*
		 * Required for HTTP BASIC Authorization
		 */
		$this->Security->loginOptions = array(
			'type'=>'basic',
			#'realm'=>'realm'
		);
		$this->Security->loginUsers = array(
			'soap_conb'=>'ydSp7248',
			'soap_csys'=>'sR9rMpy5'
		);
		$this->Security->validatePost = false;	# Required for non-model field submissions
		$this->Security->requireLogin();
		/*
		 * End of HTTP BASIC Authorization code
		 */
		
		/*
		 * This beforeFilter block had to be created as Router::url would not work in
		 * the class variable. 
		 * 
		 * var $wsdl = Router::url('/',true)
		 */
		#$this->wsdl = 'https://m2mnpbh.test.systor.st/services/NpcdbService?wsdl';
		$this->wsdl = Router::url('/',true) . 'wsdl/NpcdbService/sp2npcdb.wsdl';
		$this->wsdl_options=array(
			'trace'=>1,
			#'location'=>'https://m2mnpbh.test.systor.st/services/NpcdbService',
			'location'=>Router::url( array('controller'=>'services', 'action'=>'npcdb'), true),
			'login'=>'soap_conb',
			'password'=>'ydSp7248',
			'cache_wsdl'=>'WSDL_CACHE_NONE',
		);
		
	}
	
	
	function index() {
		$this->autoRender = false;
	}
	
	
	function npcdb() {
		try {
			$this->layout = false;
			$this->autoRender = false;
			$this->RequestHandler->respondAs('xml');
			$server = new SoapServer($this->wsdl, $this->wsdl_options);
			$server->SetClass("ServicesController");
			$server->handle();
		}catch( Exception $e ) {
			$this->log( print_r( $e->getMessage(), true ), 'debug' );
			throw new SoapFault("ServiceError", "Something went wrong with the service action!");
		}
	}
	
	
	function SendNpRequest($data) {
		return $this->SendNpRequestAck();
	}
	
	
	function SendNpRequestAck() {
		$obj = new stdClass();
		$obj->ServiceType = 'F';
		$obj->MessageCode = 'NpRequestAck';
		$obj->Number = 17500110;
		$obj->PortID = 'CONB-BTCF-20110526-162815';
		$obj->SubmissionID = 'CONB-2011-12345678';
		$obj->DonorID = 'BTCF';
		$obj->RecipientID = 'CONN';
		$obj->OriginationID = 'CSYS';
		$obj->DestinationID = 'CONB';
		return $obj;
	}
	
	
	function SendNpRequestAccept() {
		return $this->SendNpRequestAcceptAck();
	}
	
	
	function SendNpRequestAcceptAck() {
		$obj = new stdClass();
		$obj->ServiceType = 'F';
		$obj->MessageCode = 'NpRequestAcceptAck';
		$obj->Number = 17500110;
		$obj->PortID = 'CONB-BTCF-20110526-162815';
		$obj->SubmissionID = 'CONB-2011-12345678';
		$obj->DonorID = 'BTCF';
		$obj->RecipientID = 'CONB';
		$obj->OriginationID = 'CONB';
		$obj->DestinationID = 'CSYS';
		return $obj;
	}
	
	
}
?>