<?php
class ConXController extends AppController {

	var $name = 'ConX';
	#var $helpers = array('Html', 'Form');
	var $components = array('RequestHandler');
	var $uses = '';
	
	# WSDL Specific Options
	var $wsdl = '';
	var $wsdl_options = array();
	
	
	function beforeFilter() {
		/*
		 * This beforeFilter block had to be created as Router::url would not work in
		 * the class variable. 
		 * 
		 * var $wsdl = Router::url('/',true)
		 */
		$this->wsdl = 'https://m2mnpbh.test.systor.st/services/NpcdbService?wsdl';
		$this->wsdl = Router::url('/',true) . 'wsdl/NpcdbService/sp2npcdb.wsdl';
		$this->wsdl_options=array(
			'trace'=>1,
			'location'=>'https://m2mnpbh.test.systor.st/services/NpcdbService',
			#'location'=>Router::url( array('controller'=>'services', 'action'=>'npcdb'), true),
			'login'=>'soap_conx',
			'password'=>'pYbd8472',
			'cache_wsdl'=>'WSDL_CACHE_NONE',
		);
		
	}
	
	
	function index() {
		$this->autoRender = false;
	}
	
	
	function NpRequest() {
		try{
			$this->autoRender = false;
			
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpRequest",
				"Number" => "16500000",
				"SubmissionID" => "CONX-2011-06140001",
				#"SimCardNumber" => '891234567890123456',
				"DonorID" => "CONB",
				"RecipientID" => "CONX",
				"CompanyFlag" => "Y",
				"CommercialRegNumber" => "17398",
				"CPR" => "548945631",
				"Comments" => "No real comments here",
				"OriginationID" => "CONX",
				"DestinationID" => "CSYS",
			);
			debug( $params );
			$send = $client->SendNpRequest($params);
			debug( $send );
			
		}catch(Exception $e){
			debug( $e->detail );
		}
	}
	
	
	function NpRequestAccept() {
		try {
			$this->autoRender = false;

			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				'ServiceType' => 'F',
				'MessageCode' => 'NpRequestAccept',
				'Number' => '16550011',
				'PortID' => 'CONB-CONX-20110629-00000',
				'SubmissionID' => 'CONB-2011-06290001',
				'DonorID' => 'CONX',
				'RecipientID' => 'CONB',
				'OriginationID' => 'CONX',
				'DestinationID' => 'CSYS',
			);
			debug($params);
			$send = $client->SendNpRequestAccept($params);
			debug( $send );
		}catch( Exception $e ){
			debug( $e->detail );
		}
	}
	
	
	function NpExecute() {
		try {
			$this->autoRender = false;
	
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpExecute",
				"Number" => "17500110",
				"PortID" => "CONB-BTCF-20110525-11263", #
				"DonorID" => "BTCF",
				"RecipientID" => "CONB",
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			debug( $params );
			$send = $client->SendNpExecute($params);
			debug( $send );
		}catch( Exception $e ){
			debug( $e->detail );
		}
	}


	function NpQuery() {
		try{
			$this->autoRender = false;
			
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpQuery",
				"DateFrom" => "201101010000",
				"DateTo" => "201201010000",
				"OperatorID" => "CONX",
				"NumberFrom" => "16550000",
				"NumberTo" => "16599999",
				"Comments" => "No real comments here",
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			
			debug( $params );
			$send = $client->SendNpQuery($params);
			debug( $send );
		}catch(Exception $e){
			debug( $e->detail );
		}
	}
	
	
}

?>
