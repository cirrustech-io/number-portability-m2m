<?php
class NpcdbController extends AppController {

	var $name = 'Npcdb';
	var $components = array('RequestHandler');
	var $uses = '';
	
	# WSDL Specific Options
	var $wsdl = '';
	var $wsdl_options = array();
	

	function beforeFilter() {
		/*
		 * This beforeFilter block had to be created as Router::url would not work in
		 * the class variable. 
		 * 
		 * var $wsdl = Router::url('/',true)
		 */
		$this->wsdl = Router::url('/',true) . 'wsdl/SpService/npcdb2sp.wsdl';
		$this->log( $this->wsdl, 'debug' );
		$this->wsdl_options=array(
			'trace'=>1,
			#'location'=>'https://m2mnpbh.test.systor.st/services/NpcdbService',
			'location'=>Router::url( array('controller'=>'spservice', 'action'=>'service'), true),
			#'location'=>'https://np.2connectbahrain.com/spservice/service',
			'login'=>'soap_csys',
			'password'=>'sR9rMpy5',
			'cache_wsdl'=>'WSDL_CACHE_NONE',
		);
		$this->log( $this->wsdl_options, 'debug' );
		
	}
	
	
	function index() {
		$this->autoRender = false;
	}
	
	
	function HandleNpRequest() {
		try{
			$this->autoRender = false;
			
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				'ServiceType' => 'F',
				'MessageCode' => 'NpRequest',
				'Number' => '16511020',
				'SubmissionID' => 'CONB-2011-12345678',
				'PortID' => 'CONB-CONX-20110526-162815',
				'DonorID' => 'CONX',
				'RecipientID' => 'CONB',
				'CompanyFlag' => 'Y',
				'CommercialRegNumber' => '17398',
				'Comments' => 'No real comments here',
				'OriginationID' => 'CSYS',
				'DestinationID' => 'CONB',
			);
			debug( $params );
			$send = $client->HandleNpRequest($params);
			debug( $send );
		}catch(Exception $e){
			debug( $e->getMessage() );
		}
	}
	
	
	function NpRequestAccept() {
		try {
			$this->autoRender = false;

			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpRequestAccept",
				"Number" => "17500110",
				"PortID" => "CONB-BTCF-20110526-16281",
				"SubmissionID" => "CONB-2011-12345678",
				"DonorID" => "BTCF",
				"RecipientID" => "CONB",
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			debug($params);
			$send = $client->SendNpRequestAccept($params);
			debug( $send );
		}catch( Exception $e ){
			debug( $e->detail );
		}
	}
	
	
	function NpExecute() {
		try {
			$this -> render('test');
	
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpExecute",
				"Number" => "17500110",
				"PortID" => "CONB-BTCF-20110525-11263", #
				"DonorID" => "BTCF",
				"RecipientID" => "CONB",
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			debug( $params );
			$send = $client->SendNpExecute($params);
			debug( $send );
		}catch( Exception $e ){
			debug( $e->detail );
		}
	}


	function NpQuery() {
		try{
			$this->autoRender = false;
			
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpQuery",
				"DateFrom" => "2011-01-01",
				"DateTo" => "2012-01-01",
				"OperatorID" => "CONB",
				"NumberFrom" => "80081160",
				"NumberTo" => "80081184",
				"Comments" => "No real comments here",
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			
			debug( $params );
			$send = $client->SendNpQuery($params);
			debug( $send );
		}catch(Exception $e){
			debug( $e->detail );
		}
	}
	
	function HandleNpQueryComplete() {
		try{
			$this->autoRender = false;
			
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				'ServiceType' => 'F',
				'MessageCode' => 'NpQueryComplete',
				'Number' => '16511020',
				'SubmissionID' => 'CONB-2011-12345678',
				'PortID' => 'CONB-CONX-20110526-162815',
				'DonorID' => 'CONX',
				'RecipientID' => 'CONB',
				'CompanyFlag' => 'Y',
				'CommercialRegNumber' => '17398',
				'Comments' => 'No real comments here',
				'OriginationID' => 'CSYS',
				'DestinationID' => 'CONB',
			);
			debug( $params );
			$send = $client->HandleNpQueryComplete($params);
			debug( $send );
		}catch(Exception $e){
			debug( $e->getMessage() );
		}
	}
	
	
	function HandleNpExecuteBroadcast() {
		try {
			#$this -> render('test');
	
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			$params = array(
				"ServiceType" => "M",
				"MessageCode" => "NpExecuteBroadcast",
				"Number" => "12345678",
				"PortID" => "CONB-CONX-20110927-99900", #
				"DonorID" => "CONX",
				"RecipientID" => "CONB",
				'NewRoute' => 'b01',
				'PortingDatetime' => '2011-07-17T13:08:45.320+03:00',
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			$this->log( $params, 'debug' );
			$send = $client->HandleNpExecuteBroadcast($params);
			$this->log( $send, 'debug' );
		}catch( Exception $e ){
			debug( $e->getMessage() );
			debug( $e->detail );
		}
	}
	
	function HandleNpDeactivateBroadcast() {
		try {
			#$this -> render('test');
			#debug( 'test' );
			#exit;
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			/*$params = array(
				"ServiceType" => "M",
				"MessageCode" => "NpExecuteBroadcast",
				"Number" => "12345678",
				"PortID" => "CONB-CONX-20110927-99900", #
				"DonorID" => "CONX",
				"RecipientID" => "CONB",
				'NewRoute' => 'b01',
				'PortingDatetime' => '2011-07-17T13:08:45.320+03:00',
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);*/
			$params = array(
				'ServiceType' => 'F',
				'MessageCode' => 'NpDeactivateBroadcastAck',
				'Number' => '12345678',
				'PortID' => 'CONB-CONX-20110927-99900',
				'SubscriptionNetworkID' => 'CONB',
				'BlockID' => 'CONX',
				'OriginationID' => 'CONB',
				'DestinationID' => 'CSYS',
			);
			$this->log( 'NPCDB: HandleNpDeactivateBroadcast', 'debug' );
			$this->log( $params, 'debug' );
			$send = $client->HandleNpDeactivateBroadcast($params);
			$this->log( $send, 'debug' );
		}catch( Exception $e ){
			debug( $e->getMessage() );
			debug( $e->detail );
		}
	}
}

?>