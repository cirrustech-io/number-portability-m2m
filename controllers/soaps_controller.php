<?php
class SoapsController extends AppController {

	var $name = 'Soaps';
	#var $helpers = array('Html', 'Form');
	var $components = array('RequestHandler');
	
	function index() {
		//
	}
	
	function service() {
		$this->layout = false;
		$this->autoRender = false;
		#$this->RequestHandler->respondAs('xml');
		ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
		#Configure::write('debug', 0);
		$wsdl = "http://localhost/cake/np/wsdl/stock-quotes.wsdl";
		$options=array(	'trace'=>1 );
		$server = new SoapServer($wsdl, $options);
		$server->SetClass("SoapsController");
		#$this->file_write(  "Working...\n" );
		$server->handle();
		#debug( "Starting File.\n" );
	}
	
	
	function getQuote($symbol) { 
		#global $quotes; 
		$quotes = array( 
  			"ibm" => 98.33,
			"cisco" => 87.19
		);  
		return $quotes[$symbol];
		#return array($symbol => $quotes[$symbol]);
	} 
	
	function client() {
		#$this->render('index');
		$this->layout = false;
		$this->autoRender = false;
		#$this->RequestHandler->respondAs('xml');
		ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
		$wsdl = "http://localhost/cake/np/wsdl/stock-quotes.wsdl";
		$client = new SoapClient( $wsdl );
		print $client->getQuote("ibm") . "\n";  
		print $client->getQuote("cisco") . "\n";  
	}
	
	private function file_write($data) {
		$filename = '/tmp/soap.log';
		$handle = fopen($filename, 'a');
		fwrite( $handle, $data . "\n" );
		fclose( $handle );
		
	}
	
	function clear() {
		#$this->render('index');
		$this->layout = false;
		$this->autoRender = false;
		Cache::clear();
		debug( "Clearing Cache... \n" );
	}
	
	
}
?>