<?php
class MessagesController extends AppController {

	var $name = 'Messages';
	var $helpers = array('Html', 'Form');
	var $components = array('RequestHandler');
	
	# WSDL Specific Options
	var $wsdl = '';
	var $wsdl_options = array();
	
	
	function beforeFilter() {
		/*
		 * This beforeFilter block had to be created as Router::url would not work in
		 * the class variable. 
		 * 
		 * var $wsdl = Router::url('/',true)
		 */
		#$this->wsdl = 'https://m2mnpbh.test.systor.st/services/NpcdbService?wsdl';
		$this->wsdl = Router::url('/',true) . 'wsdl/NpcdbService/sp2npcdb.wsdl';
		$this->wsdl_options=array(
			'trace'=>1,
			#'location'=>'https://m2mnpbh.test.systor.st/services/NpcdbService',
			'location'=>'https://m2m.npcs.bh/services/NpcdbService?wsdl',
			#'location'=>Router::url( array('controller'=>'services', 'action'=>'npcdb'), true),
			'login'=>'soap_conb',
			'password'=>'ydSp7248',
			'cache_wsdl'=>'WSDL_CACHE_NONE',
		);
		
	}
	
	
	function index() {
		$this->autoRender = false;
	}
	
	
	function NpRequest() {
		try{
			$this->autoRender = false;
			
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpRequest",
				"Number" => "12345678",
				"SubmissionID" => "CONB-2011-07060001",
				#"SimCardNumber" => '891234567890123459',
				"DonorID" => "CONX",
				"RecipientID" => "CONB",
				"CompanyFlag" => "Y",
				"CommercialRegNumber" => "12345/0",
				"CPR" => "123456789",
				"Comments" => "No real comments here",
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			debug( $params );
			$send = $client->SendNpRequest($params);
			debug( $send );
			
		}catch(Exception $e){
			debug( $e->detail );
		}
	}
	
	
	function NpRequestAccept() {
		try {
			$this->autoRender = false;

			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpRequestAccept",
				"Number" => "16501111",
				"PortID" => "BTCF-CONB-20111211-00042",
				"SubmissionID" => "BTCF-2011-00000184",
				"DonorID" => "CONB",
				"RecipientID" => "BTCF",
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			debug($params);
			$send = $client->SendNpRequestAccept($params);
			debug( $send );
		}catch( Exception $e ){
			debug( $e->detail );
		}
	}
	
	
	function NpExecute() {
		try {
			$this->autoRender = false;
	
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			$params = array(
				'ServiceType' => 'F',
				'MessageCode' => 'NpExecute',
				'Number' => '16550001',
				'PortID' => 'CONB-CONX-20110614-00003', #
				'DonorID' => 'CONX',
				'RecipientID' => 'CONB',
				'OriginationID' => 'CONB',
				'DestinationID' => 'CSYS',
			);
			debug( $params );
			$send = $client->SendNpExecute($params);
			debug( $send );
		}catch( Exception $e ){
			debug( $e->detail );
		}
	}
	
	
	function NpExecuteComplete() {
		try {
			$this->autoRender = false;
	
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			$params = array(
				'ServiceType' => 'F',
				'MessageCode' => 'NpExecuteComplete',
				'Number' => '16501111',
				'PortID' => 'BTCF-CONB-20111211-00042', #
				'DonorID' => 'CONB',
				'RecipientID' => 'BTCF',
				'OriginationID' => 'CONB',
				'DestinationID' => 'CSYS',
			);
			debug( $params );
			$send = $client->SendNpExecuteComplete($params);
			debug( $send );
		}catch( Exception $e ){
			debug( $e->detail );
		}
	}


	function NpQuery() {
		try{
			$this->autoRender = false;
			
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				"ServiceType" => "F",
				"MessageCode" => "NpQuery",
				"DateFrom" => "2000-01-01T00:00:00+03:00",
				#"DateTo" => "2011-06-31T00:00:00+03:00",
				"OperatorID" => "CONX",
				"NumberFrom" => "16550000",
				"NumberTo" => "16599999",
				"Comments" => "No real comments here",
				"OriginationID" => "CONB",
				"DestinationID" => "CSYS",
			);
			
			debug( $params );
			$send = $client->SendNpQuery($params);
			debug( $send );
		}catch(Exception $e){
			debug( $e->detail );
		}
	}
	
	
	function NpRequestCancel() {
		try {
			$this->autoRender = false;
			
			$client = new SoapClient($this->wsdl, $this->wsdl_options);
			
			$params = array(
				'ServiceType' => 'F',
				'MessageCode' => 'NpRequestCancel',
				'Number' => '16550005',
				'PortID' => 'CONB-CONX-20110614-00001',
				'SubmissionID' => 'CONB-2011-12345691',
				'DonorID' => 'CONX',
				'RecipientID' => 'CONB',
				'OriginationID' => 'CONB',
				'DestinationID' => 'CSYS',
			);
			
			debug( $params );
			$send = $client->SendNpRequestCancel($params);
			debug( $send );	
		}catch(Exception $e) {
			debug( $e->detail );
		}
	}
	
}

?>
