<?php
class PortedNumbersController extends AppController {

	var $name = 'PortedNumbers';
	var $helpers = array('Html', 'Form');

	function insert() {
		/*
		Add a New Ported Number in to the MySQL database.
		*/
		$count = $this->PortedNumber->find('count', array(
			'conditions' => array(
				'PortedNumber.port_id' => $this->data->PortID 
		) ) );
		
		if (!empty($this->data) && $count==0) {
			$enum_data = array(
				'port_id' => $this->data->PortID,
				'service_type' => $this->data->ServiceType,
				'number' => $this->data->Number,
				'subscription_network' => $this->data->RecipientID,
				'donor_id' => $this->data->DonorID,
				'routing_number' => $this->data->NewRoute,
				'port_datetime' => $this->data->PortingDatetime			
			);
			$enum = array( 'PortedNumber' => $enum_data );

			$this->PortedNumber->create();
			if ($this->PortedNumber->save($enum)) {
				$this->log( 'MySQL: Record added to database table.', 'debug');
				return true;
			} else {
				$this->log( 'MySQL: Insert failure.', 'debug');
				return false;
			}
		} else {
			$this->log( 'MySQL: Failed - No input data, or the record already exists.', 'debug');
			return false;
		}
		return false;
	}
	
	function remove() {
		/*
		Delete a Ported Number in to the MySQL database.
		*/
		
		$this->log( 'REMOVE: ' . print_r($this->data, true), 'debug' );
		
		$number = $this->data->Number;
		$this->log( 'REMOVE - Number: ' . $number, 'debug' );
		
		if (!$number) {
			$this->log( 'MySQL: Delete - Number not specified.', 'debug' );
			return False;
		}

		if ($this->PortedNumber->delete($number)) {
			$this->log( 'MySQL: Delete - Successfully deleted ' . $number, 'debug' );
			return True;
		}else{
			$this->log( 'MySQL: Delete - Unidentified Error.', 'debug' );
		}
		
		return false;
	}
}
?>